const fs = require("fs");
const path = require("path");

function readSourseFile(filePath1) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath1, "utf-8", function (error, data) {
      if (error) {
        return reject(error);
      } else {
        resolve(data);
      }
    });
  });
}
function upperCaseData(filePath2, data) {
  let upperCaseFile = data.toUpperCase();
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath2, upperCaseFile, function (error, data) {
      if (error) {
        return reject(error);
      } else {
        return resolve();
      }
    });
  });
}

function writeFileName(allFilesPath, filePath2) {
  return new Promise((resolve, reject) => {
    fs.writeFile(allFilesPath, filePath2, function (error, data) {
      if (error) {
        return reject(error);
      } else {
        return resolve();
      }
    });
  });
}

function readUpperCaseData(filePath2) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath2, "utf-8", function (error, data) {
      if (error) {
        reject(error);
      } else {
        resolve(data);
      }
    });
  });
}

function splitSentence(filePath3, data) {
  let lowerCaseData = data.toLowerCase().split(".");
  let sentence = lowerCaseData.join("\n");
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath3, sentence, function (error, data) {
      if (error) {
        reject(error);
      } else {
        resolve(data);
      }
    });
  });
}

function appendFileName(allFilesPath, filePath2) {
  return new Promise((resolve, reject) => {
    fs.appendFile(allFilesPath, "\n" + filePath2, function (error, data) {
      if (error) {
        return reject(error);
      } else {
        return resolve();
      }
    });
  });
}

function readSentence(filePath3) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath3, "utf-8", function (error, data) {
      if (error) {
        reject(error);
      } else {
        resolve(data);
      }
    });
  });
}

function sortSentence(filePath4, data) {
  let sentenceArray = data.split("\n");
  function compareFn(name1, name2) {
    if (name1 > name2) {
      return 1;
    }
    if (name1 < name2) {
      return -1;
    } else {
      return 0;
    }
  }
  sentenceArray.sort(compareFn);
  let sentenceString = sentenceArray.join("\r\n");
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath4, sentenceString, function (error, data) {
      if (error) {
        reject(error);
      } else {
        resolve();
      }
    });
  });
}

function readAllFileNames(allFilesPath) {
  return new Promise((resolve, reject) => {
    fs.readFile(allFilesPath, "utf-8", function (error, data) {
      if (error) {
        return reject(error);
      } else {
        return resolve(data);
      }
    });
  });
}

function deleteEachFile(file) {
  return new Promise((resolve, reject) => {
    fs.unlink(file, function (error, data) {
      if (error) {
        reject(error);
      } else {
        resolve("data");
      }
    });
  });
}

function deleteAllFileNames(data) {
  let fileNameArray = data.split("\n");
  const promisesArray = fileNameArray.map((eachFile) => {
    return deleteEachFile(eachFile);
  });
  return promisesArray;
  // return new Promise((resolve, reject) => {
  //   const promisesArray = fileNameArray.map((eachFile) => {
  //     return deleteEachFile(eachFile);
  //   });
  //   resolve(promisesArray);
  // });
}
function problem2() {
  let filePath1 = path.join(__dirname, "lipsum.txt");
  let filePath2 = path.join(__dirname, "upperCaseData.txt");
  let filePath3 = path.join(__dirname, "sentence.txt");
  let filePath4 = path.join(__dirname, "sortSentence.txt");
  let allFilesPath = path.join(__dirname, "filenames.txt");
  readSourseFile(filePath1)
    .then((data) => {
      console.log(data);
      console.log(`Successfully read data in ${filePath1}`);
      return upperCaseData(filePath2, data);
    })
    .then((data) => {
      console.log(`Successfully written upper case data in ${filePath2}`);
      return writeFileName(allFilesPath, filePath2);
    })
    .then((data) => {
      console.log(`File name ${filePath2} added to ${allFilesPath}`);
      return readUpperCaseData(filePath2);
    })
    .then((data) => {
      console.log(`Successfully read upper case data in ${filePath2}`);
      return splitSentence(filePath3, data);
    })
    .then((data) => {
      console.log(`Successfully written sentence in ${filePath3}`);
      return appendFileName(allFilesPath, filePath3);
    })
    .then((data) => {
      console.log(`File name ${filePath2} added to ${allFilesPath}`);
      return readSentence(filePath3);
    })
    .then((data) => {
      console.log(`Successfully read data in ${filePath3}`);
      return sortSentence(filePath4, data);
    })
    .then((data) => {
      console.log(`Successfully read sorted data in ${filePath4}`);
      return appendFileName(allFilesPath, filePath4);
    })
    .then((data) => {
      console.log(`File name ${filePath2} added to ${allFilesPath}`);
      return readAllFileNames(allFilesPath);
    })
    .then((data) => {
      console.log(`Successfully read filenames in ${allFilesPath}`);
      return deleteAllFileNames(data);
    })

    .then((data) => {
      console.log(data);
      Promise.all(data).then(() => {
        console.log("Deleted all files");
      });
    })
    .catch((error) => {
      console.log(error);
    });
}

module.exports = problem2;
