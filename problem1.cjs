const fs = require("fs");
const path = require("path");

function createDirectory(folderPath) {
  return new Promise((resolve, reject) => {
    fs.mkdir(folderPath, (error, data) => {
      if (error && error.code !== "EEXIST") {
        console.error(error);
        reject(error);
      } else {
        if (error && error.code === "EEXIST") {
          console.log(error);
        } else {
          console.log("Directory created successfully");
        }
        resolve(data);
      }
    });
  });
}

function createFile(eachFile) {
  let jsonPath = path.join(__dirname, `jsonFile ${eachFile}.json`);
  return new Promise((resolve, reject) => {
    fs.writeFile(jsonPath, "", function (err, data) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function createRandomFiles(randomValues) {
  let createJsonFiles = randomValues.map((eachfile) => {
    return createFile(eachfile);
  });
  return createJsonFiles;
}

function deleteFile(eachFile) {
  let jsonPath = path.join(__dirname, `jsonFile ${eachFile}.json`);
  return new Promise((resolve, reject) => {
    fs.unlink(jsonPath, function (err, data) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function deleteRandomFiles(randomValues) {
  let deleteJsonFiles = randomValues.map((eachfile) => {
    return deleteFile(eachfile);
  });
  return deleteJsonFiles;
}

function problem2() {
  let randomValueArray = [0, 0, 0, 0, 0];
  let previous = [];
  function generateRandomValue() {
    let eachRandomValue = Math.floor(Math.random() * 10);
    if (previous.includes(eachRandomValue)) {
      return generateRandomValue();
    } else {
      previous.push(eachRandomValue);
      return eachRandomValue;
    }
  }
  let randomValues = randomValueArray.map((each) => {
    let value = generateRandomValue();
    return value;
  });
  let folderPath = path.join(__dirname, "jsonFiles");
  createDirectory(folderPath)
    .then((data) => {
      console.log("Directory created successfully");
      return createRandomFiles(randomValues);
    })
    .then((data) => {
      return Promise.all(data);
    })
    .then(() => {
      console.log("Created Random Json files successfully");
      return deleteRandomFiles(randomValues);
    })

    .then((data) => {
      return Promise.all(data);
    })
    .then(() => {
      console.log("Deleted Random Json files successfully");
    })
    .catch((error) => {
      console.log(error);
    });
}
module.exports = problem2;
